# GeNERACE ここから

## gree/unity-webview.git

forkしてきました

## Why

* JSのcallbackで実装したくなかったから
* AndroidのWebViewを拡張したかったから

iOSはplugins/iOS/WebView.mmを直接いじればいいので、android向けにWebViewClient.javaの機能を拡張しPluginをビルドすることを目的とした。

もっといい方法があったらm(__)m

## build

	$ brew install android-sdk
	$ brew install ant


sdkのupdate

	$ android

taget id を確認。何も表示されない場合はandroid-sdkで解決する。

	$ android list targets
	
project.properties で target=android-18だったのでid: 1 がandroid-18 になるようにした。


##### plugins/Android/install.sh

AndroidPlayerのpathを確認する

多分unity4時代？

	UNITYLIBS="/Applications/Unity/Unity.app/Contents/PlaybackEngines/AndroidPlayer/bin/classes.jar"

手元のunity5でpathを確認した

	UNITYLIBS="/Applications/Unity/Unity.app/Contents/PlaybackEngines/AndroidPlayer/release/bin/classes.jar"


build

	$ cd plugins/Android
	$ ./install.sh

build/Packager/Assets/Plugins/Android/WebViewPlugin.jar が出来ました。


# GeNERACE ここまで

### Introduction

unity-webview is a plugin for Unity that attaches WebView component in the game scene. It works on Android, iOS, OS X, and **WebPlayer**.

unity-webview is derived from keijiro-san's unity-webview-integration https://github.com/keijiro/unity-webview-integration .

### Caution ###
** This plugin doesn't support Unity 3.*. **
But you can be able to run in Unity 3, if modified WebPlayerTemplates/unity-webview/index.html.

### How to use

(TBW)

### Document

(TBW)


### For iOS

CADisplayLink stops updating when UIWebView scrolled, thus you have to change AppController.mm as the following.

    -        [_displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    +        [_displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    
### For Web Player

Since using IFRAME, some problems can be occurred because of browsers' XSS prevention. It is desired that "an_unityplayer_page.html…" and "a_page_loaded_in_webview.html…" are located at same domain.


### Android: Uncaught TypeError: Object [object Object] has no method 'call'

https://github.com/gree/unity-webview/issues/10

### Sample Project

    $ open sample/Assets/Sample.unity
    $ open dist/unity-webview.unitypackage
    Import all files
